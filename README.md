# loopback-API Base Node JS
api-customer test Crud
```
git clone https://gitlab.com/mfauzann/api-costumer.git
cd api-costumer
npm install
npm start atau node .
```
cara setting database masuk folder server/datasource.json 

## Dockumentasi 

### 1. Cara Get data

Request URL : http://localhost:3000/api/Customers/get

Request URL : http://localhost:3000/api/Customers

### 2. Cara Post data

Request URL : http://localhost:3000/api/Customers/insert

format Parameter Type data Array JSON

-Single Record
```
[
    {
      "data":{     
        "cust_name":"string",
	      "cust_address":"string",
	      "cust_phone": "0"
      }
    }
]

-multiple record

[
    {
      "data":{     
        "cust_name":"string",
	      "cust_address":"string",
	      "cust_phone": "0"
      }
    },
     {
      "data":{     
        "cust_name":"string",
	      "cust_address":"string",
	      "cust_phone": "0"
      }
    }
]
```

### 3. Cara Put data
Request URL : http://localhost:3000/api/Customers/update

format Parameter Type data Array JSON

```
-Single Record

[
    {
      "cust_id":0,
      "data":{     
        "cust_name":"string",
	      "cust_address":"string",
	      "cust_phone": "0"
      }
    }
]
-multiple record
[
    {
      "cust_id":0,
      "data":{     
        "cust_name":"string",
	      "cust_address":"string",
	      "cust_phone": "0"
      }
    },
     {
      "cust_id":0,
      "data":{     
        "cust_name":"string",
	      "cust_address":"string",
	      "cust_phone": "0"
      }
    }
]
```

### 4. Cara delete data
Request URL : http://localhost:3000/api/Customers/delete/{id}

http://localhost:3000/api/Customers/delete/3  <--- Contoh  Prameter id

#### App info

- Name: `api-costumer`
- Dir to contain the project: `api-costumer`

