module.exports = function(Customer) {
    const enabledRemoteMethods = ["find", "prototype.replaceAttributes"];
    var isStatic = true;
    Customer.sharedClass.methods().forEach(method => {
      const methodName = method.stringName.replace(/.*?(?=\.)/, '').substr(1);
      if (enabledRemoteMethods.indexOf(methodName) === -1) {
        Customer.disableRemoteMethodByName(methodName, isStatic);
      }
    });

    Customer.remoteMethod(
        'updateDataMoreThanOne',  
        {
            description: 'Update data bisa satu record atau lebih',
            accepts:[
                { arg: 'filter', type: 'array', http: { source: 'body' }}
            ],
            returns: {
                arg: 'Result', type: 'array', root: true
            },
            http: { path: '/update', verb: 'put' }
        }
    );
    Customer.remoteMethod(
        'insertDataMoreThanOne',  
        {
            description: 'insert data bisa satu record atau lebih',
            accepts:[
                { arg: 'filter', type: 'array', http: { source: 'body' }}
            ],
            returns: {
                arg: 'Result', type: 'array', root: true
            },
            http: { path: '/insert', verb: 'post' }
        }
    );

    Customer.remoteMethod(
        'getData',  
        {
            description: 'Get data',
            accepts:[
                { arg: 'filter', type: 'string'}
            ],
            returns: {
                arg: 'res', type: 'object', root: true
            },
            http: { path: '/get', verb: 'get' }
        }
    );
    Customer.remoteMethod(
        'deleteData',  
        {
            description: 'delete data',
            accepts:[
                { arg: 'id', type: 'string'}
            ],
            returns: {
                arg: 'res', type: 'object', root: true
            },
            http: { path: '/delete/:id', verb: 'delete' }
        }
    );

    Customer.updateDataMoreThanOne =  async function(filter) {
        const hslAll = [];

        if(filter.length < 1){
            let err =  new Error("data id kurang dari satu!")
            err.statusCode = 400;   
            throw err;
        }

        for(let record of filter){
            const data = record.data;
            const id = record.cust_id;
            const hsl = {};
            if(!id){
                hsl.sukses = false;
                hsl.err = "id tidak diisi.";
                hslAll.push(hsl);
                continue
            } 
            if(!data){
                hsl.sukses = false;
                hsl.err = "data tidak diisi.";
                hslAll.push(hsl);
                continue
            }
          const hslInput = await Customer.upsertWithWhere({cust_id: id},data);
          hsl.sukses = true;
          hsl.data = hslInput;
          hslAll.push(hsl);  
        }
        return hslAll;
    },

    Customer.insertDataMoreThanOne =  async function(filter) {
        const hslAll = [];

        if(filter.length < 1){
            let err =  new Error("data kurang dari satu!")
            err.statusCode = 400;   
            throw err;
        }

        for(let record of filter){
            const data = record.data;
            const hsl = {};

            if(!data){
                hsl.sukses = false;
                hsl.err = "data tidak diisi.";
                hslAll.push(hsl);
                continue
            }
          const hslInput = await Customer.create(data);
          hsl.sukses = true;
          hsl.data = hslInput;
          hslAll.push(hsl);  
        }
        return hslAll;
    },
    Customer.getData = function(filter,callback){

        var ds = Customer.dataSource;
        var sql = "SELECT * FROM t_Customer ORDER BY cust_id ASC";
        new Promise(function(resolve, reject) {
            ds.connector.execute(sql, function (err, result) {
                if(err)reject (err);
                if(result === null){
                    err = new Error("fail get or not found");
                    err.statusCode = 404;
                    reject (err);
                }
                resolve (result);
            });
        }).then(function(res){
            if (!res) callback (err);
            return callback(null, res);
        }).catch(function(err){
            callback (err);
        });
    },
    Customer.deleteData = function(id, callback) {
        new Promise(function(resolve, reject) {
            var filter = {
                where : {
                    cust_id : id
                }
            }
            Customer.destroyAll({cust_id: id}, function(err, result){ 
                if(err)reject (err);
                if(result === null){
                    err = new Error("fail get or not found");
                    err.statusCode = 404;
                    reject (err);
                }
                resolve (result);
            });            
        }).then(function(res){
            if (!res) callback (err);
            return callback(null, res);
        }).catch(function(err){
            callback (err);
        });
    }
};
