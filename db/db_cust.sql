-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 21 Okt 2019 pada 06.43
-- Versi server: 10.1.37-MariaDB
-- Versi PHP: 7.1.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_cust`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `t_Customer`
--

CREATE TABLE `t_Customer` (
  `cust_id` int(3) NOT NULL,
  `cust_name` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `cust_address` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `cust_phone` int(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `t_Customer`
--

INSERT INTO `t_Customer` (`cust_id`, `cust_name`, `cust_address`, `cust_phone`) VALUES
(1, 'fauzan', 'jakarta5', 9876453),
(2, 'ozan', 'bndung2', 98764534);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `t_Customer`
--
ALTER TABLE `t_Customer`
  ADD PRIMARY KEY (`cust_id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `t_Customer`
--
ALTER TABLE `t_Customer`
  MODIFY `cust_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
